#ifndef VIDEOOPENGLWIDGET_H
#define VIDEOOPENGLWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QImage>
#include <QQueue>

class VideoOpenGLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
{
    Q_OBJECT
public:
    explicit VideoOpenGLWidget(QWidget *parent = nullptr);

private:
    QImage mImage;
    QQueue<QImage> mQueue;

    unsigned int VBO, VAO, EBO;
    unsigned int textureVideo;

    int xshift, yshift, wvideo, hvideo;

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

signals:

public slots:
    void slotGetOneFrame(QImage img);
    void slotUpdate();
};

#endif // VIDEOOPENGLWIDGET_H
