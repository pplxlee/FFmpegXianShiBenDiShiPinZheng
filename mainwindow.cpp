#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <QDebug>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->mainToolBar->setVisible(false);
    ui->statusBar->setVisible(false);

#ifdef USE_VIDEO_WIDGET
    ui->videoOpenGLWidget->setVisible(false);
#endif
#ifdef USE_VIDEO_OPENGL_WIDGET
    ui->videoWidget->setVisible(false);
#endif

    mPlayer = new VideoPlayer;
#ifdef USE_VIDEO_WIDGET
    connect(mPlayer, &VideoPlayer::sig_GetOneFrame, ui->videoWidget, &VideoWidget::slotGetOneFrame);
    connect(&mTimer, &QTimer::timeout, ui->videoWidget, &VideoWidget::slotUpdate);
#endif
#ifdef USE_VIDEO_OPENGL_WIDGET
    connect(mPlayer, &VideoPlayer::sig_GetOneFrame, ui->videoOpenGLWidget, &VideoOpenGLWidget::slotGetOneFrame);
    connect(&mTimer, &QTimer::timeout, ui->videoOpenGLWidget, &VideoOpenGLWidget::slotUpdate);
#endif


    mPlayer->start();
    mTimer.setTimerType(Qt::PreciseTimer);
    mTimer.start(20);
}

MainWindow::~MainWindow()
{
    delete ui;
}
