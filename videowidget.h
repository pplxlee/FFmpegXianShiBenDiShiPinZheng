#ifndef VIDEOWIDGET_H
#define VIDEOWIDGET_H

#include <QWidget>
#include <QImage>
#include <QQueue>
#include <QPaintEvent>

class VideoWidget : public QWidget
{
    Q_OBJECT
public:
    explicit VideoWidget(QWidget *parent = nullptr);

private:
    QImage mImage;
    QQueue<QImage> mQueue;

protected:
    void paintEvent(QPaintEvent *event);

signals:

public slots:
    void slotGetOneFrame(QImage img);
    void slotUpdate();
};

#endif // VIDEOWIDGET_H
