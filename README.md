# 使用OpenGL显示FFmpeg解码得到的视频帧示例

#### 注意：本工程仅仅作为技术可行性验证，仅仅测试了FFmpeg解码得到的数据在QOpenGLWidget上显示的可行性，并不考虑效率上的优化和代码的可重用性。

#### 可以优化的点：应该不用ffmpeg将解码得到的帧从yuv420格式转换为rgb，而是直接利用OpenGL的着色器语言进行转换。另外，渲染时，每帧都新建纹理很低效，应该只新建一次，之后每帧更新即可。

 **要求** ：win7以上，Qt5.3以上。

 **用法** ：需要下载ffmpeg3.4.x版本的动态库文件和头文件并放到工程目录中，具体做法如下：

1. 在工程目录中新建文件夹 libs/ffmpeg
2. 在 https://ffmpeg.zeranoe.com/builds/ 分别下载ffmpeg3.4.x的shared和dev包，分别解压。
3. 将dev包解压得到的lib文件夹和include文件夹都拷贝到步骤1中新建的文件夹ffmpeg中，再将shared中的bin文件夹中的所有dll文件拷贝到libs/ffmpeg/lib文件夹中。

这样就可以编译运行看到效果了。

ffmpeg解码部分参考了：http://blog.csdn.net/leixiaohua1020/article/details/38868499