#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>
#include "videoplayer.h"
#include <QPaintEvent>
#include <QQueue>
#include <QTimer>
#include <QTime>


//#define USE_VIDEO_WIDGET

#ifndef USE_VIDEO_WIDGET
#define USE_VIDEO_OPENGL_WIDGET
#endif

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QImage mImage;
    QQueue<QImage> mQueue;
    QTimer mTimer;
    VideoPlayer *mPlayer;
    QTime mTime;

};

#endif // MAINWINDOW_H
