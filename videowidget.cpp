#include "videowidget.h"
#include <QPainter>
#include <QDebug>

VideoWidget::VideoWidget(QWidget *parent) : QWidget(parent)
{

}

void VideoWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    if(mImage.size().width() <= 0) return;

    ///将图像按比例缩放成和窗口一样大小
    QImage img = mImage.scaled(this->size(),Qt::KeepAspectRatio);

    int x = this->width() - img.width();
    int y = this->height() - img.height();

    x /= 2;
    y /= 2;

    painter.drawImage(QPoint(x,y),img); //画出图像
}

void VideoWidget::slotGetOneFrame(QImage img)
{
    mQueue.enqueue(img);
}

void VideoWidget::slotUpdate()
{
    if(mQueue.isEmpty())    return;
    mImage = mQueue.dequeue();
    update(); //调用update将执行 paintEvent函数
}
